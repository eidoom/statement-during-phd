DOCNAME=statement

all: $(DOCNAME).pdf

.PHONY: clean view

$(DOCNAME).pdf: $(DOCNAME).tex $(DOCNAME).bib Makefile
	latexmk -pdf $<

view: $(DOCNAME).pdf
	evince $< &

clean:
	latexmk -C
	-rm *.bbl *Notes.bib *-blx.bib *.xml
